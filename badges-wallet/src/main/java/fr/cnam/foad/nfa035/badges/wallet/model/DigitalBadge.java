package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.*;
import java.util.*;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge extends InputStream implements Comparable<DigitalBadge> {



    protected DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;




    /**
     * Constructeur du DigitalBadge
     *
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge, Date begin, Date end, String serial) {
        this.metadata = metadata;
        this.badge = badge;
        this.begin=begin;
        this.end=end;
        this.serial=serial;

    }


    public DigitalBadge(String s, Date begin, Date end, DigitalBadgeMetadata metadata, Object serial) {
        super();
    }


    /**
     * Méthode de comparaison
     * @param
     * @return le résultat de comparaison
     */

    @Override
    public int compareTo(DigitalBadge o ) {


        DigitalBadge a = this;
        DigitalBadge b = o;


        if(a.metadata.getBadgeId()>b.metadata.getBadgeId()){
            return 1;}
        else if(a.metadata.getBadgeId()<b.metadata.getBadgeId()){
            return -1;}


        return 0;
    }


    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadge)) return false;
        DigitalBadge that = (DigitalBadge) o;
        return Objects.equals(getMetadata(), that.getMetadata()) && Objects.equals(getBadge(), that.getBadge()) && Objects.equals(getEnd(), that.getEnd()) && Objects.equals(getSerial(), that.getSerial());
    }




    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }


    /**
     * Getter du début
     * @return date de début
     */
    public Date getBegin() {
        return begin;
    }

    /**
     *  Getter de la date de fin
     * @return date de fin
     */
    public Date getEnd() {
        return end;
    }


    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Getter du code de série unique
     * @return code de série unique
     */

    public String getSerial() {
        return serial;
    }


    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(getMetadata(), getBadge(), getEnd(), getSerial());
    }

    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }


    /**
     * Setter de la date de début
     * @param begin
     */
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    /**
     * Setter de la date de fin
     * @param end
     */
    public void setEnd(Date end) {
        this.end = end;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }


    /**
    * Setter du code de série unique
    * @param serial
    */

    public void setSerial(String serial) {
        this.serial = serial;
    }


    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ", end=" + end +
                ", serial='" + serial + '\'' +
                '}';
    }

    /**
     * lecture des octets du flux
     * @return -1  à la fin du flux
     * @throws IOException
     */
    @Override
    public int read() throws IOException {

        int flux = 0;
     do{

          Reader reader = new StringReader(toString());

          for (int i = 0; i< toString().length(); i++) {
              flux = reader.read();
          }

      }while (flux != -1);

      return -1;
    }
}