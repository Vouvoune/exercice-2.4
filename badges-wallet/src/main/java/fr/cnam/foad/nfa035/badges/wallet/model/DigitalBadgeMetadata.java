package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

/**
 * POJO model représentant les Métadonnées d'un Badge Digital
 */
public class DigitalBadgeMetadata implements Comparable<DigitalBadgeMetadata>{

    private int badgeId;
    private long walletPosition;
    private long imageSize;



    /**
     * Constructeur
     *
     */
    public DigitalBadgeMetadata(int badgeId, long walletPosition, long imageSize) {
        this.badgeId = badgeId;
        this.walletPosition = walletPosition;
        this.imageSize = imageSize;
    }

    public DigitalBadgeMetadata() {
        super();
    }



    /**
     * Méthode de comparaison
     * @param
     * @return le résultat de comparaison
     */
    @Override
    public int compareTo(DigitalBadgeMetadata o) {
        DigitalBadgeMetadata a = this;
        DigitalBadgeMetadata b = o;


        if (a.getBadgeId() > b.getBadgeId()) {
            return 1;
        } else if (a.getBadgeId() < b.getBadgeId()) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DigitalBadgeMetadata)) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return getBadgeId() == that.getBadgeId() && getWalletPosition() == that.getWalletPosition() && getImageSize() == that.getImageSize();
    }

    /**
     * Getter du badgeId
     * @return l'id du badge
     */
    public int getBadgeId() {
        return badgeId;
    }

    /**
     * Getter de la taille de l'image
     * @return long la taille de l'image
     */
    public long getImageSize() {
        return imageSize;
    }

    /**
     * Getter de la position du badge dans le wallet
     * @return long la position du badge dans le wallet
     */
    public long getWalletPosition() {
        return walletPosition;
    }


    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(getBadgeId(), getWalletPosition(), getImageSize());
    }

    /**
     * Setter du badgeId
     * @param badgeId
     */
    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }



    /**
     * Setter de l'imageSize
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }



    /**
     * Setter de la position du badge dans le wallet
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }


    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeId=" + badgeId +
                ", walletPosition=" + walletPosition +
                ", imageSize=" + imageSize +
                '}';
    }



}





