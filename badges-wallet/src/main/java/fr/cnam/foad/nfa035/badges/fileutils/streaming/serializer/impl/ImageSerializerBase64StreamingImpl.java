package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 */
public class ImageSerializerBase64StreamingImpl
        extends AbstractStreamingImageSerializer<File, AbstractImageFrameMedia> {

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }

    @Override
    public void serialize(File source, WalletFrameMedia media) throws IOException {
        getSourceInputStream(source).transferTo(getSerializingStream((AbstractImageFrameMedia) media));
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64OutputStream(media.getEncodedImageOutput());
    }

}
