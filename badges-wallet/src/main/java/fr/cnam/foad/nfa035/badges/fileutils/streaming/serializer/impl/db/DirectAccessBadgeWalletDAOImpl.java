package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.IOException;
import java.io.OutputStream;

public interface DirectAccessBadgeWalletDAOImpl {
    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException;


}
